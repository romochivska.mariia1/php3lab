<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }

    table, th, td {
        border: 1px solid black;
    }

    th, td {
        padding: 8px;
        text-align: left;
    }
</style>

<a href="<?php echo e(route('zapravkas.create')); ?>">Create Zapravka</a> <br>

<?php if(count($zapravkas) > 0): ?>
    <table>
        <thead>
        <tr>
            <th>Fullname</th>
            <th>Address</th>
            <th>Fuel</th>
            <th>Fuel Price</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $zapravkas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $zapravka): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($zapravka->fullname); ?></td>
                <td><?php echo e($zapravka->address); ?></td>
                <td><?php echo e($zapravka->fuel); ?></td>
                <td><?php echo e($zapravka->fuelprice); ?></td>
                <td>
                    <form action="<?php echo e(route('zapravkas.destroy', $zapravka->id)); ?>" method="post" style="display: inline;">
                        <?php echo csrf_field(); ?>
                        <?php echo method_field('DELETE'); ?>
                        <button type="submit">Delete</button>
                    </form>

                    <a href="<?php echo e(route('zapravkas.edit', $zapravka->id)); ?>">Edit</a>
                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
<?php else: ?>
    <p>No data available.</p>
<?php endif; ?>
<?php /**PATH C:\xampp\htdocs\lab3\laravel\resources\views/zapravkas/index.blade.php ENDPATH**/ ?>